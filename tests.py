from copy import deepcopy
from unittest import TestCase
from unittest.mock import patch, Mock, ANY

from pony.orm import db_session, rollback
from vk_api.bot_longpoll import VkBotMessageEvent, VkBotEvent


import settings
from bot import BotVK
from generate_ticket import generate_ticket


def isolate_db(test_func):
    def wrapper(*args, **kwargs):
        with db_session:
            test_func(*args, **kwargs)
            rollback()
    return wrapper


class Test1(TestCase):
    RAW = {'group_id': 149019148, 'type': 'message_new',
           'event_id': 'df615e80c52024311381ee5177a6e48ec2373a0d', 'v': '5.131',
           'object': {'message':
                          {'date': 1682439723,
                           'from_id': 11630216, 'id': 62, 'out': 0, 'attachments': [],
                           'conversation_message_id': 62, 'fwd_messages': [],
                           'important': False, 'is_hidden': False,
                           'peer_id': 11630216, 'random_id': 0,
                           'text': 'Привет'},
                      'client_info': {'button_actions': ['text', 'vkpay', 'open_app', 'location',
                                                         'open_link', 'open_photo', 'callback',
                                                         'intent_subscribe', 'intent_unsubscribe'],
                                      'keyboard': True, 'inline_keyboard': True, 'carousel': True,
                                      'lang_id': 0}}}

    def test_run(self):
        count = 5
        event = [{}] * count # [{}, {}, {} ...]
        long_poller_listen_mock = Mock()
        long_poller_listen_mock.listen = Mock(return_value=event)
        with patch('bot.VkApi'):
            with patch('bot.VkBotLongPoll', return_value=long_poller_listen_mock):
                vkbot = BotVK("", "")
                vkbot.on_event = Mock()
                vkbot.send_image = Mock()
                vkbot.run()

                vkbot.on_event.assert_called()
                vkbot.on_event.assert_any_call({})
                assert vkbot.on_event.call_count == count


    INPUTS = [
        'Привет',
        'А когда?',
        'Где будет конференция?',
        'Зарегистрируй меня',
        'Вениамин',
        'мой адрес email@email',
        'email@email.ru'
    ]

    EXPECTED_OUTPUT = [
        settings.DEFAULT_ANSWER,
        settings.INTENTS[0]['answer'],
        settings.INTENTS[1]['answer'],
        settings.SCENARIOS['registration']['steps']['step1']['text'],
        settings.SCENARIOS['registration']['steps']['step2']['text'],
        settings.SCENARIOS['registration']['steps']['step2']['failure_text'],
        settings.SCENARIOS['registration']['steps']['step3']['text'].format(name='Вениамин', email='email@email.ru')
    ]

    @isolate_db
    def test_run_ok(self):
        send_mock = Mock()
        api_mock = Mock()
        api_mock.messages.send = send_mock

        events = []
        for input_text in self.INPUTS:
            event = deepcopy(self.RAW)
            event['object']['message']['text'] = input_text
            events.append(VkBotEvent(event))

        long_poller_mock = Mock()
        long_poller_mock.listen = Mock(return_value=events)

        with patch('bot.VkBotLongPoll', return_value=long_poller_mock):
            bot = BotVK('', '')
            bot.api = api_mock
            bot.send_image = Mock()
            bot.run()

        assert send_mock.call_count == len(self.INPUTS)

        real_outputs = []
        for call in send_mock.call_args_list:
            args, kwargs = call
            real_outputs.append(kwargs['message'])
        assert real_outputs == self.EXPECTED_OUTPUT


    def test_image_generation(self):

        with open('files/asas@asas.ru.png', 'rb') as avatar_file:
            avatar_mock = Mock()
            avatar_mock.content = avatar_file.read()

        with patch('requests.get', return_value=avatar_mock):
            ticket_file = generate_ticket('Алекс', 'asas@asas.ru')

        with open('files/ticket_example.png', 'rb') as expected_file:
            expected_bytes = expected_file.read()

        assert ticket_file.read() == expected_bytes

    # def test_on_event(self):
    #     event = VkBotMessageEvent(raw=self.RAW)
    #
    #     send_mock = Mock()
    #
    #     with patch('bot.VkApi'):
    #         vkbot = BotVK("", "")
    #         # vkbot.api = Mock()
    #         vkbot.api.messages.send = send_mock
    #         """
    #         переопределение vkbot.api.messages.send на мок функцию
    #         которая потом вызывается в on_event с параметрами event
    #         """
    #         vkbot.on_event(event)
    #
    #     send_mock.assert_called_once_with(
    #         user_id=self.RAW['object']['message']['peer_id'],
    #         random_id=ANY,
    #         message=self.RAW['object']['message']['text'])
    #     """
    #     проверяем вызов подставленной вместо vkbot.api.messages.send
    #     МОК функции send_mock
    #     """
