#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pprint import pprint
from random import randint
import logging

import requests
from pony.orm import db_session
from vk_api import VkApi
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
#Импорт токена
import handlers
from models import UserState, Registration

try:
    import settings
except ImportError:
    exit('необходимо создать совой файл настроек вместо settings.py.default и назвать settings.py')



# vk_session = vk_api.VkApi('+71234567890', 'mypassword')
# vk_session.auth()
#
# vk = vk_session.get_api()
#
# print(vk.wall.post(message='Hello world!'))


log = logging.getLogger("Bot")


def logger():
    log.setLevel(logging.DEBUG)
    bot_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(bot_formatter)
    stream_handler.setLevel(logging.INFO)
    log.addHandler(stream_handler)

    file_handler = logging.FileHandler("bot.log", encoding="UTF-8")
    file_handler.setFormatter(bot_formatter)
    file_handler.setLevel(logging.DEBUG)
    log.addHandler(file_handler)

# class UserState:
#     """
#     Состояние пользователя внутри сценария
#     """
#     def __init__(self, scenario_name, step_name, context=None):
#         self.scenario_name = scenario_name
#         self.step_name = step_name
#         self.context = context or {}


class BotVK:
    """
    Echo bot для vk.com

    Сценарий регистрации на конференцию "Название" через vk.com.
    Use python 3.10

    Поддерживает ответы ответы на вопросы про дату, место проведения и сценарий регистрации:
    - спрашиваем имя
    - спрашиваем email
    - говорим об успешной регистрации
    Если шаг не пройден, задаем уточняющий вопрос пока шаг не будет пройден
    """

    def __init__(self, group_id, token):
        """

        :param group_id: номер группы в vk.com
        :param token:   секретный токен присвоенный для доступа
        """
        self.group_id = group_id
        self.token = token
        self.vk_session = VkApi(token=token)
        self.long_poller = VkBotLongPoll(self.vk_session, self.group_id, wait=25)
        self.api = self.vk_session.get_api()
        self.user_states = dict()   # peer_id -> UserState

    def run(self):
        """
        Запуск бота
        :return: None
        """
        for event in self.long_poller.listen():
            log.debug("событие получено")
            try:
                self.on_event(event)
            except Exception as err:
                log.exception(err)

    @db_session
    def on_event(self, event):
        """
        Отправляет сообщение назад, если это текст.

        :param event: VkBotMessageEvent object
        :return: None
        """

        if event.type != VkBotEventType.MESSAGE_NEW:
            log.debug("мы пока не умеем обрабатывать сообщения такого типа %s", event.type)
            return

        user_id = event.object.message['peer_id']
        text = event.object.message['text']

        state = UserState.get(user_id=str(user_id))

        if state is not None:
            self.continue_scenario(text, state, user_id)
        else:
            # search intent
            for intent in settings.INTENTS:
                log.debug(f'User gets {intent}')
                if any(token in text.lower() for token in intent['tokens']):
                    if intent['answer']:
                        self.send_text(intent['answer'], user_id)
                    else:
                        self.start_scenario(user_id, intent['scenario'], text)
                    break
            else:
                self.send_text(settings.DEFAULT_ANSWER, user_id)

    def send_text(self, text_to_send, user_id):
        self.api.messages.send(
            user_id=user_id,
            random_id=randint(0, 2 ** 20),
            message=text_to_send)

    def send_image(self, image, user_id):
        upload_url = self.api.photos.getMessagesUploadServer()['upload_url']
        upload_data = requests.post(url=upload_url, files={'photo': ('image.png', image, 'image/png')}).json()
        image_data = self.api.photos.saveMessagesPhoto(**upload_data)
        owner_id = image_data[0]['owner_id']
        media_id = image_data[0]['id']
        attachment = f'photo{owner_id}_{media_id}'

        self.api.messages.send(
            user_id=user_id,
            random_id=randint(0, 2 ** 20),
            attachment=attachment)

    def send_step(self, step, user_id, text, context):
        if 'text' in step:
            self.send_text(step['text'].format(**context), user_id)
        if 'image' in step:
            handler = getattr(handlers, step['image'])
            image = handler(text, context)
            self.send_image(image, user_id)

    def start_scenario(self, user_id, scenario_name, text):
        scenario = settings.SCENARIOS[scenario_name]
        first_step = scenario['first_step']
        step = scenario['steps'][first_step]
        self.send_step(step, user_id, text, context={})
        UserState(user_id=str(user_id), scenario_name=scenario_name, step_name=first_step, context={})

    def continue_scenario(self, text, state, user_id):
        steps = settings.SCENARIOS[state.scenario_name]['steps']
        step = steps[state.step_name]

        handler = getattr(handlers, step['handler'])
        if handler(text=text, context=state.context):
            # next step
            next_step = steps[step['next_step']]
            self.send_step(next_step, user_id, text, state.context)

            if next_step['next_step']:
                # switch to next step
                state.step_name = step['next_step']
            else:
                # finish scenario

                log.info('Зарегистрирован: {name} {email}'.format(**state.context))
                Registration(name=state.context['name'], email=state.context['email'])
                state.delete()
        else:
            # retry current step
            text_to_send = step['failure_text'].format(**state.context)
            self.send_text(text_to_send, user_id)

# <<class 'vk_api.bot_longpoll.VkBotMessageEvent'>(
# {'group_id': 149019148, 'type': 'message_new', 'event_id': 'd385d629dc74c3fbd2c3520acb9131526fc894f0', 'v': '5.131',
#       'object': {
#           'message': {
#               'date': 1681814361, 'from_id': 11630216, 'id': 24, 'out': 0, 'attachments': [], 'conversation_message_id': 24, 'fwd_messages': [], 'important': False, 'is_hidden': False, 'peer_id': 11630216, 'random_id': 0, 'text': 'Ард'},
#               'client_info': {
#                   'button_actions': ['text', 'vkpay', 'open_app', 'location', 'open_link', 'open_photo', 'callback', 'intent_subscribe', 'intent_unsubscribe'],
#                   'keyboard': True, 'inline_keyboard': True, 'carousel': True, 'lang_id': 0}}})>

if __name__ == "__main__":
    logger()
    vkbot = BotVK(settings.GROUP_ID, settings.TOKEN)
    vkbot.run()



