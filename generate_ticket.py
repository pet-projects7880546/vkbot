import requests
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont

TEMPLATE_PATH = 'files/ticket_base.JPG'
FONT_PATH = 'files/Roboto-Regular.ttf'
FONT_SIZE = 20
BLACK = (0, 0, 0, 255)
NAME_OFFSET = (256, 200)
EMAIL_OFFSET = (256, 230)
AVATAR_SIZE = (120, 120)
AVATAR_OFFSET = (50, 175)

def generate_ticket(name, email):
    base = Image.open(TEMPLATE_PATH).convert('RGBA')
    fnt = ImageFont.truetype(FONT_PATH, FONT_SIZE)

    drow = ImageDraw.Draw(base)
    drow.text(NAME_OFFSET, name, font=fnt, fill=BLACK)
    drow.text(EMAIL_OFFSET, email, font=fnt, fill=BLACK)

    response = requests.get(url=f'https://robohash.org/{email}')
    avatar_file = BytesIO(response.content)
    avatar = Image.open(avatar_file).convert('RGBA').resize(AVATAR_SIZE)
    base.paste(avatar, AVATAR_OFFSET)

    temp_file = BytesIO()
    base.save(temp_file, 'png')
    temp_file.seek(0)

    # base.show()
    return temp_file


# generate_ticket('Алекс', 'asas@asas.ru')